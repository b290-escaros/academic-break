// Create a simple function that will use switch and if else statements
shipItem = (item, weight, location) => {
  let shippingFee = 0;

  // Shipping fee based on weight
  if (weight === 3) {
    shippingFee = 150;
  } else if (weight >= 5 && weight < 8) {
    shippingFee = 280;
  } else if (weight >= 8 && weight < 10) {
    shippingFee = 340;
  } else if (weight >= 10) {
    shippingFee = 410;
  } else if (weight > 10) {
    shippingFee = 560;
  }

  // Additional charge based on location using switch
  switch (location) {
    case "local":

      break;
    case "international":
      shippingFee += 250; 
      break;
    default:
      console.log("Invalid location");
      return;
  }

  const totalShippingFee = shippingFee;
  return `The total shipping fee for ${item} that will ship to ${location} is P${totalShippingFee}`;
}
// Invoke the functions
console.log(shipItem("Bag", 3, "local"));
console.log(shipItem("Sneaker", 8, "international"));



// Create a simulation of adding product in a cart using constructors with methods and array manipulation
const Product = function(name, price, quantity) {
  this.name = name;
  this.price = price;
  this.quantity = quantity;
};

const Customer = function(name) {
  this.name = name;
  this.cart = [];

  this.addToCart = (product) => {
    this.cart.push(product);
    return `${product.name} is added to cart.`;
  };

  this.removeFromCart = (product) => {
    const index = this.cart.findIndex(item => item.name === product.name);
    if (index !== -1) {
      this.cart.splice(index, 1);
      return `${product.name} is removed from the cart.`;
    }
    return `${product.name} is not found in the cart.`;
  };
};


const newProduct = new Product("Car", 10, 2);
const newCustomer = new Customer("Robin");

// Invoke methods of the customer
console.log(newCustomer.addToCart(newProduct));
console.log(newCustomer.removeFromCart(newProduct));
